var r = require("../routes");

export const apiService = {
    get,
};

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
};

function get() {


    return fetch(r.cors.concat(r.api))
        .then(handleErrors)
        .then(response => response.json());
};