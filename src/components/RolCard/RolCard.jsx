import React, { Component } from 'react';
import { Card, Row, Toast } from 'react-bootstrap';
import Style from './RolCard.module.css';
import LazyLoad from 'react-lazyload';
import Pacman from 'react-spinners/PacmanLoader';
import ReactStoreIndicator from 'react-score-indicator';
// import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import bikeImg from './../../media/images/bike.png'



class RolCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            friend: null
        };
    }

    onFriendClick() {
        if (this.state.friend) {
            let e = this.state.friend[0];
            return (
                <Toast style={{ height: "150px" }} onClose={() => this.setState({ friend: null })}>
                    <Toast.Header>
                        <Row>
                            <img src={e.thumbnail} height={50} className="rounded mr-2" alt={e.name} />
                            <strong className="mr-auto">{e.name}</strong>
                        </Row>
                        <small>{e.age + " years"}</small>
                    </Toast.Header>
                    <Toast.Body>Let's create a social Gnome network!</Toast.Body>
                </Toast>
            );
        } else {
            return;
        }
    }

    render() {

        const { inhabitant } = this.props;
        return (
            <LazyLoad once offset={200} placeholder={<Row><Pacman color="orangered" css={{ margin: "auto", marginTop: "10%" }} /></Row>}>
                {this.onFriendClick()}
                <Card className={`${Style.card} "bg-dark text-white"`} key={inhabitant.id}>
                    <Card.Img variant="top" src={bikeImg} alt={inhabitant.stationName} />
                    <Card.Body className={Style.cardBody}>
                        <Card.Title>{inhabitant.stationName}</Card.Title>
                        {/* <Card.Text> */}
                        <div className={Style.Stats}>
                            <div style={{ width: "50%", margin: "auto" }}>
                                <ReactStoreIndicator
                                    value={inhabitant.availableDocks}
                                    maxValue={inhabitant.totalDocks}
                                    width={75}
                                />
                            </div>
                            {/* <div style={{ width: "50%" }}>
                                    <CircularProgressbar
                                        percentage={70}
                                        text={inhabitant.availableDocks.toString().concat(" Bikes")}
                                    />
                                </div> */}

                        </div>
                        {/* </Card.Text> */}
                    </Card.Body>
                    <Card.Footer className={Style.CardFooter} style={{ color: inhabitant.statusValue === "In Service" ? "Green" : "Red" }}>
                        <span>{inhabitant.statusValue}</span>
                        {/* <span>{inhabitant.availableDocks.toString().concat("/").concat(inhabitant.totalDocks.toString())}</span> */}
                    </Card.Footer>
                </Card>
            </LazyLoad>
        );
    }
}

export default RolCard;