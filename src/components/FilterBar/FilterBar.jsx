import React, { Component } from 'react';
import { FormControl, InputGroup, Card, Row, Accordion } from 'react-bootstrap';
import Style from './FilterBar.module.css';
import { Button } from 'react-bootstrap';

class FilterBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
        this.onChange = this.onChange.bind(this);
        this.onClear = this.onClear.bind(this);
    }

    onClear(e) {
        this.props.onClear();
    }

    onChange(e, f) {
        var name, value;
        if (e) {
            name = e.target.name;
            value = e.target.value;
        } else if (f) {
            name = f.target.name;
            value = f.target.text;
        }
        this.props.onChange(value, name);
    }

    render() {
        return (
            <>
                <Accordion defaultActiveKey="1" style={{ margin: "1rem" }}>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Filters
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div>
                                <div style={{ margin: "1rem" }}>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>Name</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl name="name" onChange={this.onChange} />
                                    </InputGroup>
                                </div>
                                <div className={Style.FilterContainer}>
                                    <div className={Style.FilterGroup}>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Min available bikes</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="minBike" onChange={this.onChange} />
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Max available bikes</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="maxBike" onChange={this.onChange} />
                                        </InputGroup>

                                    </div>
                                    <div className={Style.FilterGroup}>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Min Dock</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="minDock" onChange={this.onChange} />
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Max Dock</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="maxDock" onChange={this.onChange} />
                                        </InputGroup>
                                    </div>

                                </div>
                                <Row style={{ width: "inherit", margin: "auto" }}>
                                    <Button size="sm" style={{ width: "100%" }} onClick={this.onClear} >Clear</Button>
                                </Row>
                            </div>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </>
        );
    }
}

export default FilterBar;