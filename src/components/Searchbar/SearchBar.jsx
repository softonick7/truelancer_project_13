import React from 'react';
import { Form, Button } from 'react-bootstrap';
import style from './SearchBar.module.css';

class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            search: ""
        }
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({
            search: e.target.value
        })
    }

    render() {
        return (
            <Form inline className={style.SearchBar} onSubmit={(e) => this.props.onSearch(this.state.search)}>
                <input className={style.input1} type="text" placeholder="Search" onChange={this.onChange} value={this.state.search} />
                <Button id="SearchBtn" type="submit" className={style.SearchBtn} />
            </Form>
        )
    }
}

export default SearchBar;
