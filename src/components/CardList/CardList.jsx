import React, { Component } from 'react';
import RolCard from './../RolCard/RolCard';
import { CardDeck, Image } from 'react-bootstrap';
import Style from './CardList.module.css';
// import {LazyLoad} from 'react-lazyload';


class CardList extends Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        let inhabitants = this.props.inhabitantsFiltered || null;
        inhabitants = (inhabitants && inhabitants.length > 0) ? inhabitants.map((inhabitant) => {
            return (
                <RolCard key={inhabitant.id} onFriendClick={this.onFriendClick} inhabitants={this.props.inhabitants} inhabitant={inhabitant} />
            );
        }) : <div> <Image src="http://www.tistabene.com/images/no-result-found.png" alt="Empty results" className={Style.NotFoundLogo} /></div>;

        return (
            <CardDeck style={{ justifyContent: "center" }}>
                {inhabitants}
            </CardDeck>
        );
    }
}

export default CardList;