import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import NavHeader from './components/NavHeader/NavHeader';
import Footer from './components/Footer/Footer';
import Home from './pages/Home';
import Inhabitants from './pages/Inhabitants';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlay, faPause, faForward, faBackward } from "@fortawesome/free-solid-svg-icons";
import { faWhatsapp, faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';

library.add(
  faPlay,
  faPause, 
  faForward,
  faBackward,
  faWhatsapp,
  faFacebook, 
  faTwitter
);

class App extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  render(){
    window.scrollTo(0, 0);
    return (
        <Router onChange = {() => window.scrollTo(0, 0)} className ="test">
          <NavHeader fixed = "top" history={this.props.history}/>
          <div className="App">
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/List/:id" component={Inhabitants}></Route>
          </div>
          <Footer fixed = "bottom"/>
        </Router>
    );
  }
}

export default App;
