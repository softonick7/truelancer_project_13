module.exports = {

    /* ROUTES */
    inhabitantsList: (name) => {
        return `/list/${name}`
    },

    /* API */
    cors: "https://cors-anywhere.herokuapp.com/",
    api: "https://feeds.citibikenyc.com/stations/stations.json",
}