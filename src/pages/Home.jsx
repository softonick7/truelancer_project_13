/**
 * Home Page.
 *
 * @version 1.0.1
 * @author [Sofian Azouache](sofian.azouache@hotmail.com)
 */

import React from 'react';
import SearchBar from '../components/Searchbar/SearchBar'
import style from './Home.module.css';
import Image from 'react-bootstrap/Image';
import homeImg from './../media/images/citibike_home.jpg'

const r = require('../routes');

class Home extends React.Component {

    constructor() {
        super();
        this.state = {
            searchText: ""
        }
        this.onSearch = this.onSearch.bind(this);
    }

    /**
     * On submit searchbar navigate to list of results
     *
     * @param {String} tileEntityId the value of the input inside searchbar
     */

    onSearch(id) {
        id = id ? id : " ";
        const path = r.inhabitantsList(id);
        this.props.history.push(path);
    }

    render() {
        return (
            <div className={style.container}>
                <div className={style.titleContainer}>
                    <h2>Search your ...</h2>
                    <Image src={homeImg} height={250} />
                </div>
                <div className={style.searchBar}>
                    <SearchBar onSearch={this.onSearch} />
                </div>
            </div>
        )
    }
}

export default Home;
