import React, { Component, lazy, Suspense } from 'react';
import { connect } from 'react-redux';
import { apiActions } from '../actions';
import FilterBar from '../components/FilterBar/FilterBar';
import Style from './Inhabitants.module.css';
import Pacman from 'react-spinners/PacmanLoader';

const CardList = lazy(() => import('../components/CardList/CardList'));

class Inhabitants extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.onChange = this.onChange.bind(this);
        this.onClear = this.onClear.bind(this);
    }

    componentDidMount() {
        const { match: { params } } = this.props;
        this.setState({ name: params.id ? params.id : null });
        this.props.get();
    }

    onClear() {
        this.setState({
            name: null,
            minBike: null,
            maxBike: null,
            minDock: null,
            maxDock: null,
            rol: null
        });
    }

    onChange(value, name) {
        this.setState({
            [name]: value
        });
    }

    onFilter() {
        let inhabitants = this.props.inhabitants.stationBeanList;

        if (!inhabitants) {
            return;
        }

        if (this.state.name !== " ") {
            inhabitants = inhabitants.filter((inhabitant) => {
                return ((inhabitant.stationName && inhabitant.stationName.toLowerCase().includes(this.state.name.toLowerCase())));
            });
        }

        if (this.state.minBike) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.availableDocks > this.state.minBike);
            });
        }

        if (this.state.maxBike) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.availableDocks < this.state.maxBike)
            });
        }

        if (this.state.minDock) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (parseFloat(inhabitant.totalDocks) > parseFloat(this.state.minDock));
            });
        }

        if (this.state.maxDock) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (parseFloat(inhabitant.totalDocks) > parseFloat(this.state.maxDock))
            });
        }

        return inhabitants;
    }

    render() {
        const inhabitantsFiltered = this.onFilter();
        const inhabitants = this.props.inhabitants.stationBeanList;
        return (
            <div className={Style.Container}>
                <h1 style={{ color: "white", marginTop: "5rem" }}>NYC Stations</h1>
                {inhabitantsFiltered &&
                    <>
                        <FilterBar onChange={this.onChange} onClear={this.onClear} />
                        <Suspense fallback={<Pacman color="orangered" size="100px" css={{ margin: "auto", marginTop: "10%" }} />}>
                            {inhabitants && <CardList inhabitantsFiltered={inhabitantsFiltered} inhabitants={inhabitants}></CardList>}
                        </Suspense>
                    </>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inhabitants: state.getApiStatus.items,
});

const mapDispatchToProps = dispatch => ({
    get: (id) => dispatch(apiActions.get(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Inhabitants);
