import {
    apiService
} from "../services";

export const apiActions = {
    get,
};

function get() {

    function request(items) {
        return {
            type: "API_GETALL_REQUEST",
            items
        };
    }

    function success(items) {
        return {
            type: "API_GETALL_SUCCESS",
            items
        };
    }

    function failure(error) {
        return {
            type: "API_GETALL_FAILURE",
            error
        };
    }

    return dispatch => {
        dispatch(request());
        apiService.get().then(
            items => {
                dispatch(success(items));
            },
            error => {
                dispatch(failure(error));
            }
        );
    };
}